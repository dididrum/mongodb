﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace _3Projekat_baze
{
    public class Knjiga
    {
        public ObjectId id { get; set; }

        public ObjectId OwnerId { get; set; }

        public string ime { get; set; }

        public string opis { get; set; }

        public string utisak { get; set; }


        public string zanr { get; set; }

        public string autor { get; set; }

    }
}
