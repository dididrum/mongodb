﻿
namespace _3Projekat_baze
{
    partial class Biblioteka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Biblioteka));
            this.stripovi = new System.Windows.Forms.Label();
            this.biografske = new System.Windows.Forms.Label();
            this.ljubavne = new System.Windows.Forms.Label();
            this.istorijskee = new System.Windows.Forms.Label();
            this.decije = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // stripovi
            // 
            this.stripovi.AutoSize = true;
            this.stripovi.BackColor = System.Drawing.Color.Transparent;
            this.stripovi.Font = new System.Drawing.Font("Papyrus", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stripovi.Location = new System.Drawing.Point(540, 160);
            this.stripovi.Name = "stripovi";
            this.stripovi.Size = new System.Drawing.Size(37, 144);
            this.stripovi.TabIndex = 17;
            this.stripovi.Text = "S\r\n T\r\n  R\r\n   I\r\n    P\r\n     O\r\n      V\r\n        I";
            this.stripovi.Click += new System.EventHandler(this.stripovi_Click);
            // 
            // biografske
            // 
            this.biografske.AutoSize = true;
            this.biografske.BackColor = System.Drawing.Color.Transparent;
            this.biografske.Font = new System.Drawing.Font("Papyrus", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.biografske.Location = new System.Drawing.Point(234, 342);
            this.biografske.Name = "biografske";
            this.biografske.Size = new System.Drawing.Size(142, 18);
            this.biografske.TabIndex = 15;
            this.biografske.Text = "B I O G R A F S K E";
            this.biografske.Click += new System.EventHandler(this.biografske_Click);
            // 
            // ljubavne
            // 
            this.ljubavne.AutoSize = true;
            this.ljubavne.BackColor = System.Drawing.Color.Transparent;
            this.ljubavne.Font = new System.Drawing.Font("Papyrus", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ljubavne.Location = new System.Drawing.Point(245, 286);
            this.ljubavne.Name = "ljubavne";
            this.ljubavne.Size = new System.Drawing.Size(111, 18);
            this.ljubavne.TabIndex = 14;
            this.ljubavne.Text = "LJ U B A V N E";
            this.ljubavne.Click += new System.EventHandler(this.ljubavne_Click);
            // 
            // istorijskee
            // 
            this.istorijskee.AutoSize = true;
            this.istorijskee.BackColor = System.Drawing.Color.Transparent;
            this.istorijskee.Font = new System.Drawing.Font("Papyrus", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.istorijskee.Location = new System.Drawing.Point(487, 148);
            this.istorijskee.Name = "istorijskee";
            this.istorijskee.Size = new System.Drawing.Size(47, 180);
            this.istorijskee.TabIndex = 13;
            this.istorijskee.Text = "I\r\n S\r\n  T\r\n   O\r\n    R\r\n     I\r\n      J\r\n       S\r\n        K\r\n         E";
            this.istorijskee.Click += new System.EventHandler(this.istorijskee_Click);
            // 
            // decije
            // 
            this.decije.AutoSize = true;
            this.decije.BackColor = System.Drawing.Color.Transparent;
            this.decije.Font = new System.Drawing.Font("Papyrus", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.decije.Location = new System.Drawing.Point(434, 148);
            this.decije.Name = "decije";
            this.decije.Size = new System.Drawing.Size(33, 96);
            this.decije.TabIndex = 12;
            this.decije.Text = "D\r\n E\r\n  C\r\n   I\r\n    J\r\n     E";
            this.decije.Click += new System.EventHandler(this.decije_Click);
            // 
            // Biblioteka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.stripovi);
            this.Controls.Add(this.biografske);
            this.Controls.Add(this.ljubavne);
            this.Controls.Add(this.istorijskee);
            this.Controls.Add(this.decije);
            this.DoubleBuffered = true;
            this.Name = "Biblioteka";
            this.Text = "addBook";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label stripovi;
        private System.Windows.Forms.Label biografske;
        private System.Windows.Forms.Label ljubavne;
        private System.Windows.Forms.Label istorijskee;
        private System.Windows.Forms.Label decije;
    }
}

