﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3Projekat_baze.Services;
using _3Projekat_baze.Models;
using _3Projekat_baze.Forms;

namespace _3Projekat_baze
{
    public partial class IstorijskeForm : Form
    {
        User currentUser;
        Knjiga currentBook;
        DBService db = new DBService();
        int batchSize = 15;
        List<CheckBox> lista = new List<CheckBox>();
        public IstorijskeForm(User user)
        {
            currentUser = user;
            InitializeComponent();
            SetMyBooks(true);
            showMore.Enabled = false;
            delete.Enabled = false;

        }

        private void SetMyBooks(bool firstLookup)
        {
            List<Knjiga> knjige = db.GetOwnedBooks(currentUser.Id, 50, firstLookup);
            listaKnjiga.DataSource = null;

            List<string> proba = new List<string>();


            foreach (var test in knjige)
            {
                if(test.zanr=="Istorijska")
                proba.Add("'" + test.ime+"'" + " - "+test.autor);


            }

            listaKnjiga.DataSource = proba;

        }



        private void searchBtn_Click(object sender, EventArgs e)
        {
            string searchText = searchTxtBox.Text;

            if (searchText == "")
            {
                SetSearchedBooks(new List<Knjiga>());
                showMore.Enabled = false;
                return;
            }

         

            List<Knjiga> knjige = db.GetBooksMatching(searchText, batchSize);

            SetSearchedBooks(knjige);

        }

        private void SetSearchedBooks(List<Knjiga> knjige)
        {
            searchTxtBox.Text = "";
            List<string> proba = new List<string>();
            foreach (var test in knjige)
            {

                if (test.zanr == "Istorijska")
                // proba.Add("'" + test.ime + "'" + " - " + test.autor);
                {

                    proba.Add("'" + test.ime + "'" + " - " + test.autor);
                    currentBook = test;



                    //box.CheckedChanged += new EventHandler(obrisiKnjigu);



                    // lista.Add(box);


                    //flowLayoutPanel1.Controls.Add(box);
                    checkedListBox1.DataSource = proba;
                }

            }
        }

        private void prikaziDugme()
        {

            delete.Enabled = true;
            showMore.Enabled = true;


        }



        private void showMore_Click(object sender, EventArgs e)
        {
            IstorijskeFormKnjiga IstorijskeKnjiga = new IstorijskeFormKnjiga(currentBook, currentUser);
            IstorijskeKnjiga.ShowDialog();
        }

      
        

        private void addBook_Click_1(object sender, EventArgs e)
        {
            Knjiga knjiga = new Knjiga() { OwnerId = currentUser.Id };

            BookForm bookNameForm = new BookForm(knjiga);
            bookNameForm.ShowDialog();

            if (bookNameForm.DialogResult == DialogResult.OK)
            {
                db.AddBook(knjiga);
                //EditController editController = new EditController(song, db);
                // EditForm editForm = new EditForm(editController);
                // editForm.ShowDialog();
                SetMyBooks(false);
            }
        }

        private void delete_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Are you sure you want to delete this book", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                db.DeleteBook(currentBook);
                MessageBox.Show("Obrisana obaveza!");
            }
            SetMyBooks(true);
            checkedListBox1.DataSource = null;
           
            //  this.cekirano=0;
            delete.Enabled = false;
            showMore.Enabled = false;
            this.obrisiEl();
        }

        private void obrisiEl()
        {
            foreach (var item in checkedListBox1.CheckedItems)
            {
                checkedListBox1.Items.Remove(item);
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = checkedListBox1.SelectedIndex;
            int count = checkedListBox1.Items.Count;
            for (int x = 0; x < count; x++)
            {
                if (index != x)
                {
                    // checkedListBox1.SetItemChecked(x, false);
                    checkedListBox1.SetItemCheckState(x, CheckState.Unchecked);
                   

                }


            }
        }

        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            this.prikaziDugme();
        }

        private void showMore_Click_1(object sender, EventArgs e)
        {
            IstorijskeFormKnjiga IstorijskeKnjiga = new IstorijskeFormKnjiga(currentBook, currentUser);
            IstorijskeKnjiga.ShowDialog();
        }
    }
}
