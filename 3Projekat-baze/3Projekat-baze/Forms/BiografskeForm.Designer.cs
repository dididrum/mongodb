﻿
namespace _3Projekat_baze.Forms
{
    partial class BiografskeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BiografskeForm));
            this.addBook = new System.Windows.Forms.Button();
            this.showMore = new System.Windows.Forms.Button();
            this.searchBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.searchTxtBox = new System.Windows.Forms.TextBox();
            this.listaKnjiga = new System.Windows.Forms.ListBox();
            this.delete = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // addBook
            // 
            this.addBook.Location = new System.Drawing.Point(12, 317);
            this.addBook.Name = "addBook";
            this.addBook.Size = new System.Drawing.Size(107, 36);
            this.addBook.TabIndex = 28;
            this.addBook.Text = "Dodaj knjigu";
            this.addBook.UseVisualStyleBackColor = true;
            this.addBook.Click += new System.EventHandler(this.addBook_Click);
            // 
            // showMore
            // 
            this.showMore.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showMore.Location = new System.Drawing.Point(316, 317);
            this.showMore.Margin = new System.Windows.Forms.Padding(2);
            this.showMore.Name = "showMore";
            this.showMore.Size = new System.Drawing.Size(132, 27);
            this.showMore.TabIndex = 27;
            this.showMore.Text = "SHOW MORE";
            this.showMore.UseVisualStyleBackColor = true;
            this.showMore.Click += new System.EventHandler(this.showMore_Click);
            // 
            // searchBtn
            // 
            this.searchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchBtn.Location = new System.Drawing.Point(339, 19);
            this.searchBtn.Margin = new System.Windows.Forms.Padding(2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(87, 28);
            this.searchBtn.TabIndex = 26;
            this.searchBtn.Text = "GO";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(224, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 31);
            this.label2.TabIndex = 25;
            this.label2.Text = "Search";
            // 
            // searchTxtBox
            // 
            this.searchTxtBox.Location = new System.Drawing.Point(230, 69);
            this.searchTxtBox.Name = "searchTxtBox";
            this.searchTxtBox.Size = new System.Drawing.Size(196, 20);
            this.searchTxtBox.TabIndex = 24;
            // 
            // listaKnjiga
            // 
            this.listaKnjiga.FormattingEnabled = true;
            this.listaKnjiga.Location = new System.Drawing.Point(12, 12);
            this.listaKnjiga.Name = "listaKnjiga";
            this.listaKnjiga.Size = new System.Drawing.Size(196, 290);
            this.listaKnjiga.TabIndex = 22;
            // 
            // delete
            // 
            this.delete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete.Location = new System.Drawing.Point(204, 317);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(107, 27);
            this.delete.TabIndex = 30;
            this.delete.Text = "OBRISI";
            this.delete.UseVisualStyleBackColor = true;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(230, 112);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(196, 169);
            this.checkedListBox1.TabIndex = 31;
            this.checkedListBox1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // BiografskeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(467, 369);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.addBook);
            this.Controls.Add(this.showMore);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchTxtBox);
            this.Controls.Add(this.listaKnjiga);
            this.Name = "BiografskeForm";
            this.Text = "BiografskeForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addBook;
        private System.Windows.Forms.Button showMore;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox searchTxtBox;
        private System.Windows.Forms.ListBox listaKnjiga;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
    }
}