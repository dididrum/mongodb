﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3Projekat_baze.Models;
using _3Projekat_baze.Services;

namespace _3Projekat_baze.Forms
{
    public partial class IstorijskeFormKnjiga : Form
    {
        Knjiga currentBook;
        User currentUser;
        DBService db = new DBService();

        public IstorijskeFormKnjiga(Knjiga knjiga, User user)
        {
            currentBook = knjiga;
            currentUser = user;
            InitializeComponent();
            SetMyBooks(true);
        }

        private void SetMyBooks(bool firstLookup)
        {
            List<Knjiga> knjige = db.GetOwnedBooks(currentUser.Id, 50, firstLookup);


            List<string> au = new List<string>();
            List<string> naz = new List<string>();
            List<string> ut = new List<string>();
            List<string> op = new List<string>();


            foreach (var test in knjige)
            {
                if (test.ime == currentBook.ime && test.zanr == "Istorijska")
                {
                    au.Add(test.autor);
                    naz.Add(test.ime);
                    ut.Add(test.utisak);
                    op.Add(test.opis);
                }




            }
            string utisakString = string.Join(",", ut);
            autor.DataSource = au;
            naziv.DataSource = naz;
            opis.DataSource = op;
            utisak.Text = utisakString;

        }

        private void commit_Click(object sender, EventArgs e)
        {
            currentBook.utisak = utisak.Text;

            utisak.Text = db.EditBook(currentBook);
            db.UpdateBook(currentBook);
            MessageBox.Show("Izmene komitovane!");
        }
    }
}
