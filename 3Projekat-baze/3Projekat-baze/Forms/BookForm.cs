﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3Projekat_baze.Models;

namespace _3Projekat_baze.Forms
{
    public partial class BookForm : Form
    {
        Knjiga knjiga;
        public BookForm(Knjiga knjiga)
        {
            this.knjiga = knjiga;
            //confirmBtn.Enabled = false;
            InitializeComponent();
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            knjiga.ime = imeTxtBox.Text;
            knjiga.opis = opisTxtBox.Text;
            knjiga.zanr = zanrComboBox.Text;
            knjiga.utisak = utisakTxtBox.Text;
            knjiga.autor = autorTxtBox.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
