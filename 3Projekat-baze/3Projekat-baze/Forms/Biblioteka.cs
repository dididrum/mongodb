﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _3Projekat_baze.Controllers;
using _3Projekat_baze.Models;
using _3Projekat_baze.Forms;
using _3Projekat_baze.Services;

namespace _3Projekat_baze
{
    public partial class Biblioteka : Form
    {
        User currentUser;
        DBService db = new DBService();
        public Biblioteka(User user)
        {
            currentUser = user;
            InitializeComponent();
        }

        private void istorijskee_Click(object sender, EventArgs e)
        {
            IstorijskeForm Istorijske = new IstorijskeForm(currentUser);
            Istorijske.ShowDialog();
        }

        private void decije_Click(object sender, EventArgs e)
        {
            DecijeForm Decije = new DecijeForm(currentUser);
            Decije.ShowDialog();
        }

        private void stripovi_Click(object sender, EventArgs e)
        {
            StripoviForm Stripovi = new StripoviForm(currentUser);
            Stripovi.ShowDialog();
        }

        private void biografske_Click(object sender, EventArgs e)
        {
            BiografskeForm Biografske = new BiografskeForm(currentUser);
            Biografske.ShowDialog();
        }

        private void ljubavne_Click(object sender, EventArgs e)
        {
            LjubavneForm Ljubavne = new LjubavneForm(currentUser);
            Ljubavne.ShowDialog();
        }
    }
}
