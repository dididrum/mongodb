﻿
namespace _3Projekat_baze.Forms
{
    partial class BookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.confirmBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.autorTxtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imeTxtBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.opisTxtBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.utisakTxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.zanrComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(116, 267);
            this.confirmBtn.Margin = new System.Windows.Forms.Padding(2);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(135, 28);
            this.confirmBtn.TabIndex = 9;
            this.confirmBtn.Text = "CONFIRM";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.confirmBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(25, 220);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "Autor:";
            // 
            // autorTxtBox
            // 
            this.autorTxtBox.Location = new System.Drawing.Point(116, 226);
            this.autorTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.autorTxtBox.Name = "autorTxtBox";
            this.autorTxtBox.Size = new System.Drawing.Size(136, 20);
            this.autorTxtBox.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 25);
            this.label1.TabIndex = 6;
            this.label1.Text = "Ime:";
            // 
            // imeTxtBox
            // 
            this.imeTxtBox.Location = new System.Drawing.Point(116, 54);
            this.imeTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.imeTxtBox.Name = "imeTxtBox";
            this.imeTxtBox.Size = new System.Drawing.Size(136, 20);
            this.imeTxtBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 86);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 25);
            this.label3.TabIndex = 11;
            this.label3.Text = "Opis:";
            // 
            // opisTxtBox
            // 
            this.opisTxtBox.Location = new System.Drawing.Point(116, 92);
            this.opisTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.opisTxtBox.Name = "opisTxtBox";
            this.opisTxtBox.Size = new System.Drawing.Size(136, 20);
            this.opisTxtBox.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(25, 129);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 25);
            this.label4.TabIndex = 13;
            this.label4.Text = "Utisak:";
            // 
            // utisakTxtBox
            // 
            this.utisakTxtBox.Location = new System.Drawing.Point(116, 135);
            this.utisakTxtBox.Margin = new System.Windows.Forms.Padding(2);
            this.utisakTxtBox.Name = "utisakTxtBox";
            this.utisakTxtBox.Size = new System.Drawing.Size(136, 20);
            this.utisakTxtBox.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 171);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 25);
            this.label5.TabIndex = 15;
            this.label5.Text = "Zanr:";
            // 
            // zanrComboBox
            // 
            this.zanrComboBox.FormattingEnabled = true;
            this.zanrComboBox.Items.AddRange(new object[] {
            "Istorijska",
            "Decija",
            "Biografija",
            "Ljubavna",
            "Strip"});
            this.zanrComboBox.Location = new System.Drawing.Point(116, 177);
            this.zanrComboBox.Name = "zanrComboBox";
            this.zanrComboBox.Size = new System.Drawing.Size(135, 21);
            this.zanrComboBox.TabIndex = 16;
            // 
            // BookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(298, 322);
            this.Controls.Add(this.zanrComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.utisakTxtBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.opisTxtBox);
            this.Controls.Add(this.confirmBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.autorTxtBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imeTxtBox);
            this.Name = "BookForm";
            this.Text = "BookForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox autorTxtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox imeTxtBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox opisTxtBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox utisakTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox zanrComboBox;
    }
}