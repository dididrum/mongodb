﻿
namespace _3Projekat_baze.Forms
{
    partial class IstorijskeFormKnjiga
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IstorijskeFormKnjiga));
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.utisak = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.opis = new System.Windows.Forms.ListBox();
            this.naziv = new System.Windows.Forms.ListBox();
            this.autor = new System.Windows.Forms.ListBox();
            this.commit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(282, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 21);
            this.label4.TabIndex = 35;
            this.label4.Text = "Utisak :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 21);
            this.label3.TabIndex = 34;
            this.label3.Text = "Opis :";
            // 
            // utisak
            // 
            this.utisak.Location = new System.Drawing.Point(357, 170);
            this.utisak.Margin = new System.Windows.Forms.Padding(2);
            this.utisak.Multiline = true;
            this.utisak.Name = "utisak";
            this.utisak.Size = new System.Drawing.Size(160, 147);
            this.utisak.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(252, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 21);
            this.label2.TabIndex = 30;
            this.label2.Text = "Naziv :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 21);
            this.label1.TabIndex = 29;
            this.label1.Text = "Autor :";
            // 
            // opis
            // 
            this.opis.FormattingEnabled = true;
            this.opis.Location = new System.Drawing.Point(16, 237);
            this.opis.Name = "opis";
            this.opis.Size = new System.Drawing.Size(311, 108);
            this.opis.TabIndex = 28;
            // 
            // naziv
            // 
            this.naziv.FormattingEnabled = true;
            this.naziv.Location = new System.Drawing.Point(332, 27);
            this.naziv.Name = "naziv";
            this.naziv.Size = new System.Drawing.Size(139, 30);
            this.naziv.TabIndex = 27;
            // 
            // autor
            // 
            this.autor.FormattingEnabled = true;
            this.autor.Location = new System.Drawing.Point(92, 27);
            this.autor.Name = "autor";
            this.autor.Size = new System.Drawing.Size(139, 30);
            this.autor.TabIndex = 26;
            // 
            // commit
            // 
            this.commit.Location = new System.Drawing.Point(442, 322);
            this.commit.Name = "commit";
            this.commit.Size = new System.Drawing.Size(75, 23);
            this.commit.TabIndex = 36;
            this.commit.Text = "Izmeni";
            this.commit.UseVisualStyleBackColor = true;
            this.commit.Click += new System.EventHandler(this.commit_Click);
            // 
            // IstorijskeFormKnjiga
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(529, 360);
            this.Controls.Add(this.commit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.utisak);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.opis);
            this.Controls.Add(this.naziv);
            this.Controls.Add(this.autor);
            this.Name = "IstorijskeFormKnjiga";
            this.Text = "IstorijskeFormKnjiga";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox utisak;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox opis;
        private System.Windows.Forms.ListBox naziv;
        private System.Windows.Forms.ListBox autor;
        private System.Windows.Forms.Button commit;
    }
}