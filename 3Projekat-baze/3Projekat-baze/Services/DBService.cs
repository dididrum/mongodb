﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using _3Projekat_baze.Models;
using _3Projekat_baze.Services;

namespace _3Projekat_baze.Services
{
    class DBService
    {
        public readonly Knjiga novaKnjiga;
        public readonly DBService dBService;
        public enum Searches
        {
            User,
            String
        }

        public DBService(Knjiga knjiga, DBService dBService = null)
        {
            novaKnjiga = knjiga;
        }

        public DBService()
        {
           
        }

        public Searches lastSearch { get; private set; }
        public string lastSearchedString { get; private set; }
        public int nextBatch { get; set; } = 0;


        public void ClearBatchIndex()
        {
            this.nextBatch = 0;
        }

        public List<Knjiga> GetBooksMatching(string searchString, int batchSize)
        {
            if (this.lastSearch != Searches.String)
            {
               // this.ClearBatchIndex();
                this.lastSearch = Searches.String;
            }
            if (this.lastSearchedString != searchString)
            {
               // this.ClearBatchIndex();
                this.lastSearchedString = searchString;
            }

            //this.nextBatch++;
            FilterDefinition<Knjiga> stringFilter = "{ ime:" + "/" + searchString + "/i }";

            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<Knjiga>("bibliotekaa");
                return collection.Find(stringFilter).ToList();
            
            }


        }

        public List<Knjiga> GetOwnedBooks(ObjectId ownerId, int batchSize, bool firstLookup)
        {
            if (this.lastSearch != Searches.User || !firstLookup)
            {
                this.ClearBatchIndex();
                this.lastSearch = Searches.User;
            }
            this.nextBatch++;
            FilterDefinition<Knjiga> userFilter = Builders<Knjiga>.Filter.Eq(knjiga => knjiga.OwnerId, ownerId);

            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<Knjiga>("bibliotekaa");
                if (collection.CountDocuments(userFilter) > --this.nextBatch * batchSize)
                {
                    return collection.Find(userFilter).Skip(batchSize * this.nextBatch++).Limit(batchSize).ToList();
                }
                else
                    return new List<Knjiga>();
            }
        }

        public User LogIn(string username, string password)
        {
            FilterDefinition<User> logInFilter = "{ Username: " + "\"" + username + "\"" + ", " + "Password: " + "\"" + password + "\"" + " }";

            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<User>("bibliotekaa");
                return collection.Find(logInFilter).ToList().Last();
            }
        }

        public bool Register(string username, string password)
        {
            FilterDefinition<User> registerFilter = "{ Username: " + "\"" + username + "\"" + " }";

            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<User>("bibliotekaa");
                if (collection.Find(registerFilter).CountDocuments() == 0)
                {
                    collection.InsertOne(new User()
                    {
                        Id = new ObjectId(),
                        Username = username,
                        Password = password
                    });
                    return true;
                }
                else
                    return false;
            }
        }

        public void AddBook(Knjiga knjiga)
        {
            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<Knjiga>("bibliotekaa");

                collection.InsertOne(knjiga);
            }
        }

        public void DeleteBook(Knjiga knjiga)
        {
            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<Knjiga>("bibliotekaa");

                collection.DeleteOne(doc => doc.id == knjiga.id);
            }
        }

        public string EditBook(Knjiga nova)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"{nova.utisak}");
         
            

            return stringBuilder.ToString();

        }

        public void UpdateBook(Knjiga nova)
        {
            using (DatabaseHandler database = new DatabaseHandler())
            {
                var collection = database.GetCollection<Knjiga>("bibliotekaa");

                collection.ReplaceOneAsync(doc => doc.id == nova.id, nova);
            }
        }

    }
}
